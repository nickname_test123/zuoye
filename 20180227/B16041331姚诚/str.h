#ifndef str_h
#define str_h
#include<stdio.h>
#include<stdlib.h>

char * strcpy(char * src){
	if (src==NULL) return NULL;
	char *tmp=src;
	int i=0;
	for(i=0;;i++){
		if(*tmp) tmp++;
		else break;
	}
	char * ret = (char *)malloc(i+1);
	tmp = ret;
	int x;
	for(x=0;x<=i;x++){
		tmp[x]=src[x];
	} 
	//src[i]=='\x00',so,use x<=i can also copy the '\x00' to the end of ret string 
	return ret;
}
#endif