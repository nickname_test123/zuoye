#include<iostream>
#include<cmath>
#include<cstring>
using namespace std;

bool isPrime(int value){
    int m = sqrt(value);
    if (value < 2) return false;
    for (int i = 2; i <= m; i++){
        if ((value%i)==0){
            return false;
        }
    }
    return true;
}

bool* GenPrime(int n){
    bool* value = new bool[n];
    memset(value,0,sizeof(bool)*n);
    value[2]=true;
    for (int i = 3; i<n; i+=2) value[i] = true; 
    //大于2的偶数均为false ,奇数先全设为true
    for (int i = 2; i <= sqrt(n); i++){
        //找到sqrt(n)以内未被置为false的数，如果是素数，则将其倍数全置为false
        if (value[i] && isPrime(i)){
            int c = 2;
            int j = i*c;
            while (j < n){
                value[j] = false;
                j = i*c++;
            }
        }
    }
    return value;
}

int main(){
    bool * p = GenPrime(1001);
    int sum=0;
    for(int i=2;i<=1000;i++){
        if(!p[i])continue;
        cout<<i<<" ";
        sum++;
        if(sum%5==0)cout<<endl;
    }
    cout<<endl;
    return 0;
}
