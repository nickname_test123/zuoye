#include <stdio.h>
int main(int argc, char **argv)
{
    if(argc == 3)
    {
        FILE *out = fopen(argv[1], 'w+');
        FILE *in = fopen(argv[2], 'r+');
        char n = NULL;
        while ((n = fgetc(in)) != EOF)
        {
            fputc(n, out);
        }
        fputc(n, out);
        fclose(in);
        fclose(out);
    }
    return 0;
}