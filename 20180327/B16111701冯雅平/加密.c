#include <stdio.h>
int main()
{
	int i;
	char s[100]="";
	printf("请输入需要加密的字符串：") ;
	gets(s);	// 键盘输入字符串
	for(i=0; s[i]!='\0'; i++)
	{
                //分别对大写字母、小写字母和数字做对应的处理
                if(s[i]>='a'&&s[i]<='z')
                     s[i]=(char)((s[i]+2-'a')%26+'a');
                if(s[i]>='A'&&s[i]<='Z')
                     s[i]=(char)((s[i]+2-'A')%26+'A');
                if(s[i]>='0'&&s[i]<='9')
                     s[i]=(char)((s[i]+2-'0')%10+'0');
	}
	puts(s);	// 输出字符串
	return 0;
}
