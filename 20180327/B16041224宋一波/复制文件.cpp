#include<stdio.h>
int main(int argc,char **argv){
	FILE *fp1=fopen(argv[1],"r");
	FILE *fp2=fopen(argv[2],"w");
	int ch;
	if(fp1==NULL||fp2==NULL){
		printf("UNABLE TO OPEN ");
		return -1;
	}
	while((ch=fgetc(fp1))!=EOF){
		fputc(ch,fp2);
	}
	fclose(fp1);
	fclose(fp2);
	return 0;
}
