#include<iostream>
using namespace std;

int nsum(unsigned int n){
	int sum=0;
	while(n){
		sum+=n%10;
		n=n/10;
	}
	return sum;
}

int main(){
	unsigned int d;
	cout<<"input:";
	cin>>d;
	int s=0;
	for(unsigned int i=1;i<=d;i++)s+=nsum(i);
	cout<<"sum:"<<s<<endl;
	return 0;
}