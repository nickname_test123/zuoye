char* Strcat(char *dst, const char *src)
{
    assert(dst!=NULL&&src!=NULL);
    char *temp = dst;
    while (*temp!='\0')
        temp++;
    while ((*temp++ = *src++) !='\0');
    return dst;
}
