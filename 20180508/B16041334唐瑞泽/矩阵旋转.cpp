#include<stdio.h>

void S(int  a[][4],int (* b)[4])
{
	int i,j;
	for(i=0;i<4;i++)
		for(j=0;j<4;j++)
		{
			b[j][3-i]=a[i][j];
		}
}

void N(int  a[][4],int (* b)[4])
{
	int i,j;
	for(i=0;i<4;i++)
		for(j=0;j<4;j++)
		{
			b[3-j][i]=a[i][j];
		}
}


int main()
{
	int a[4][4]={1,2,3,4,9,10,11,12,13,9,5,1,15,11,7,3};
	int b[4][4];
	int c[4][4];
	int x,y;
	S(a,b);
	N(a,c);
	for(x=0;x<4;x++)
	{
		for(y=0;y<4;y++)
			printf("%5d",b[x][y]);
		printf("\n");
	}
	printf("\n");
	for(x=0;x<4;x++)
	{
		for(y=0;y<4;y++)
			printf("%5d",c[x][y]);
		printf("\n");
	}
	return 0;
}