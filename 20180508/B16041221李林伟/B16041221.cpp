#include<cstdio>
using namespace std;

const int n = 4;

void print_mat(int (*mat)[n])
{
    for(int i = 0; i < n; i++)
    {
        for(int j = 0; j < n; j++)
        {
            printf("%5d", mat[i][j]);
        }
        printf("\n");
    }
    printf("\n");
}

void rotating(int (*mat)[n], int sign)
{
    int mat2[n][n];
    if(sign)
    {
        printf("����ת\n");
        for(int i = 0; i < n; i++)
        {
            for(int j = 0; j < n; j++)
            {
                mat2[i][j] = mat[n-j-1][i];
            }
        }
        print_mat(mat2);
    }
    else
    {
        printf("����ת\n");
        for(int i = 0; i < n; i++)
        {
            for(int j = 0; j < n; j++)
            {
                mat2[i][j] = mat[j][n-i-1];
            }
        }
        print_mat(mat2);
    }
}

int main()
{
    int matrix[n][n] = {{1, 2, 3, 4}, {9, 10, 11, 12}, {13, 9, 5, 1}, {15, 11, 7, 3}};
    print_mat(matrix);
    rotating(matrix, 1);
    rotating(matrix, 0);
    return 0;
}
